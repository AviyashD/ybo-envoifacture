﻿namespace OutlookMailCreator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ButtonMail = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.lblStat = new System.Windows.Forms.Label();
            this.mailToBeGenerated = new System.Windows.Forms.Label();
            this.GeneratedMail = new System.Windows.Forms.Label();
            this.lblGenerated = new System.Windows.Forms.Label();
            this.lblToBeGenerated = new System.Windows.Forms.Label();
            this.lblRemaining = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFilename = new System.Windows.Forms.TextBox();
            this.nbDeMail = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonMail
            // 
            this.ButtonMail.Location = new System.Drawing.Point(216, 162);
            this.ButtonMail.Name = "ButtonMail";
            this.ButtonMail.Size = new System.Drawing.Size(115, 23);
            this.ButtonMail.TabIndex = 0;
            this.ButtonMail.Text = "Générer";
            this.ButtonMail.UseVisualStyleBackColor = true;
            this.ButtonMail.Click += new System.EventHandler(this.ButtonMail_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(404, 65);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(26, 20);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // lblStat
            // 
            this.lblStat.AutoSize = true;
            this.lblStat.Location = new System.Drawing.Point(231, 198);
            this.lblStat.Name = "lblStat";
            this.lblStat.Size = new System.Drawing.Size(0, 13);
            this.lblStat.TabIndex = 3;
            // 
            // mailToBeGenerated
            // 
            this.mailToBeGenerated.AutoSize = true;
            this.mailToBeGenerated.Location = new System.Drawing.Point(58, 92);
            this.mailToBeGenerated.Name = "mailToBeGenerated";
            this.mailToBeGenerated.Size = new System.Drawing.Size(141, 13);
            this.mailToBeGenerated.TabIndex = 4;
            this.mailToBeGenerated.Text = "Nombre d\'e-mails à générer: ";
            // 
            // GeneratedMail
            // 
            this.GeneratedMail.AutoSize = true;
            this.GeneratedMail.Location = new System.Drawing.Point(65, 118);
            this.GeneratedMail.Name = "GeneratedMail";
            this.GeneratedMail.Size = new System.Drawing.Size(134, 13);
            this.GeneratedMail.TabIndex = 5;
            this.GeneratedMail.Text = "Nombre d\'e-mails générés: ";
            // 
            // lblGenerated
            // 
            this.lblGenerated.AutoSize = true;
            this.lblGenerated.Location = new System.Drawing.Point(213, 118);
            this.lblGenerated.Name = "lblGenerated";
            this.lblGenerated.Size = new System.Drawing.Size(0, 13);
            this.lblGenerated.TabIndex = 6;
            // 
            // lblToBeGenerated
            // 
            this.lblToBeGenerated.AutoSize = true;
            this.lblToBeGenerated.Location = new System.Drawing.Point(213, 92);
            this.lblToBeGenerated.Name = "lblToBeGenerated";
            this.lblToBeGenerated.Size = new System.Drawing.Size(0, 13);
            this.lblToBeGenerated.TabIndex = 7;
            // 
            // lblRemaining
            // 
            this.lblRemaining.AutoSize = true;
            this.lblRemaining.Location = new System.Drawing.Point(213, 141);
            this.lblRemaining.Name = "lblRemaining";
            this.lblRemaining.Size = new System.Drawing.Size(0, 13);
            this.lblRemaining.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Nombre d\'e-mails restants: ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-5, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(218, 53);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(119, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Fichier entrant:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtFilename
            // 
            this.txtFilename.Location = new System.Drawing.Point(205, 65);
            this.txtFilename.Name = "txtFilename";
            this.txtFilename.Size = new System.Drawing.Size(193, 20);
            this.txtFilename.TabIndex = 12;
            this.txtFilename.Tag = "";
            // 
            // nbDeMail
            // 
            this.nbDeMail.FormattingEnabled = true;
            this.nbDeMail.Location = new System.Drawing.Point(483, 13);
            this.nbDeMail.Name = "nbDeMail";
            this.nbDeMail.Size = new System.Drawing.Size(38, 21);
            this.nbDeMail.TabIndex = 13;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(307, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(170, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Nombre d\'e-mails générés à la fois:";
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(533, 220);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.nbDeMail);
            this.Controls.Add(this.txtFilename);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblRemaining);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblToBeGenerated);
            this.Controls.Add(this.lblGenerated);
            this.Controls.Add(this.GeneratedMail);
            this.Controls.Add(this.mailToBeGenerated);
            this.Controls.Add(this.lblStat);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.ButtonMail);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "[YBO] E-Mailing";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonSendMail;
        private System.Windows.Forms.Button ButtonMail;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label lblStat;
        private System.Windows.Forms.Label mailToBeGenerated;
        private System.Windows.Forms.Label GeneratedMail;
        private System.Windows.Forms.Label lblGenerated;
        private System.Windows.Forms.Label lblToBeGenerated;
        private System.Windows.Forms.Label lblRemaining;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFilename;
        private System.Windows.Forms.ComboBox nbDeMail;
        private System.Windows.Forms.Label label4;
    }
}

