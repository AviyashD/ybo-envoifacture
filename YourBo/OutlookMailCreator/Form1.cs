﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Diagnostics;
using System.Globalization;

namespace OutlookMailCreator
{
    public partial class Form1 : Form
    {

        public static String dataExcelPath;
        public static String dataFolder="";
        public static System.Windows.Forms.Label label1;

        public int batch = 10;
        public int numMail = 0;
        public int numRows = 0;
        public static List<Tuple<string, string, string, string, string>> ExcelResult = new List<Tuple<string, string, string, string, string>>();

        Dictionary<string, string> Mois = new Dictionary<string, string>();

        public Form1()
        {
            InitializeComponent();
            this.Text = "Envoi des factures";
            this.FormClosing += Form1_FormClosing;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (!e.Cancel)
                {
                    if ((numRows - numMail) > 0)
                    {
                        // Assume that X has been clicked and act accordingly.
                        // Confirm user wants to close
                        switch (MessageBox.Show(this, "Vous avez " + (numRows - numMail).ToString() + " mails qui ne sont pas encore générés. Voulez - vous vraiment fermer ?", "Êtes-vous sûr?", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                        {
                            //Stay on this form
                            case DialogResult.No:
                                e.Cancel = true;
                                break;
                            default:
                                break;
                        }
                    }                    
                }
            }

        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {

            try
            {
                
                //lblFilename.Text = AttachFile();
                txtFilename.Text = AttachFile();
                lblStat.Text = "";
                batch = int.Parse(nbDeMail.SelectedItem.ToString());
                numRows = 0;
                numMail = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
        private  string AttachFile()
        {
            try
            {
                OpenFileDialog attachment = new OpenFileDialog();

                attachment.Title = "Select an excel file";
                attachment.Filter = "Excel files (*.xls or .xlsx)|.xls;*.xlsx";
                attachment.ShowDialog();
                dataExcelPath = attachment.FileName;
                string fileName = attachment.SafeFileName;
                dataFolder = dataExcelPath.Replace(fileName, "");
                return fileName;
            }
            catch(Exception ex)
            {
                //  MessageBox.Show(ex.Message);
                //lblFilename.Text = "Aucun dossier n'est choisi";
                txtFilename.Text = "Aucun dossier n'est choisi";
                dataFolder = "";
                return "Aucun dossier n'est choisi";
            }

           
        }
        private void ButtonMail_Click(object sender, EventArgs e)
        {
            try
            {                
                if (dataFolder!="" && dataFolder != "Aucun dossier n'est choisi")
                {
                    ProcessExcel(dataExcelPath);

                }
                else
                {
                    MessageBox.Show("Aucun dossier n'est choisi!", "Statut dossier", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          
            //string p = getFilePath("oz");
        }

        public static string GetFilePath(string fileName)
        {
            try
            {
                var filePath = System.IO.Directory.GetFiles(@dataFolder).Where(file => file.ToLower().Contains(fileName.ToLower())).FirstOrDefault();
                return filePath;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;

            }


        }

        public static void GenerateMail(string _date, string _commande,string _client, string _mailClient, string _noFacture)
        {
            try
            {
                Outlook.Application outlookApp = new Outlook.Application();
                Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                //Outlook.Inspector oInspector = oMailItem.GetInspector;
                // Thread.Sleep(10000);


                //Add Subject
                oMailItem.Subject="[YBO SAS]["+_client+"] - ENVOI FACTURE - " + DateTime.Now.ToString("dd.MM.yyyy");

                //Date
                var finalMonthYear = "";
                var lstDate = new List<string>();
                CultureInfo ci = new CultureInfo("fr-FR");
                if (_date.Contains(','))
                {
                    lstDate = _date.Split(',').ToList();
                }
                else
                {
                    lstDate.Add(_date);
                }
                lstDate = lstDate.GroupBy(t => t).Select(y => y.First()).ToList();
                foreach (var d in lstDate)
                {
                    string getMonth = d.Substring(d.Length - 2);
                    string getYear = d.Substring(0, 4);
                    DateTime strDate = new DateTime(int.Parse(getYear), int.Parse(getMonth), 1);
                    finalMonthYear += " " + strDate.ToString("MMMM", ci) + " " + strDate.Year;
                }

                //Add Body
                oMailItem.Body =
                    "Cher Client,\r\n\n" +
                    "Vous trouverez en pièce jointe la facture relative à  nos prestations " +
                    "pour le(s) mois de " + finalMonthYear + "." +
                    "\r\n\nEn vous remerciant par avance.";

                var lstRecipient = new List<string>();
                if (_mailClient.Contains(','))
                {
                    lstRecipient = _mailClient.Split(',').ToList();
                }
                else
                {
                    lstRecipient.Add(_mailClient);
                }

                // Recipient
                Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;
                foreach (String recipient in lstRecipient)
                {
                    Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(recipient.Trim());
                    oRecip.Resolve();
                }

                //Add Attachment
                string filePath = "";
                List<string> lstAttachment = new List<string>();
                if (_noFacture.Contains(','))
                {
                    lstAttachment = _noFacture.Split(',').ToList();
                }
                else
                {
                    lstAttachment.Add(_noFacture);
                }
                lstAttachment = lstAttachment.GroupBy(t => t).Select(y => y.First()).ToList();
                foreach (string attachment in lstAttachment)
                {
                    //string attachmentN = attachment;
                    filePath = GetFilePath(attachment.Trim());
                    if (filePath != null)
                        oMailItem.Attachments.Add(filePath, Outlook.OlAttachmentType.olByValue);
                }

                //Add CC             
                Outlook.Recipient oCCRecip = oRecips.Add("clients@yourbo.fr");
                oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                oCCRecip.Resolve();

                if (oMailItem.Attachments.Count > 0)
                {
                    //Display the mailbox
                    oMailItem.Display(oMailItem);
                    oMailItem.HTMLBody = oMailItem.HTMLBody + ReadSignature();
                }
                //Outlook.Application.Quit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
        public void ProcessExcel(string fullpath)
        {

            try
            {
                lblStat.Text = "     En Cours...";
                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                Excel.Range range;
                //fullpath = "C:\\avinash\\OutlookMailCreator\\inputExcel.xlsx";
                string str;
                int rCnt;
                int cCnt;
                int rw = 0;
                int cl = 0;


                xlApp = new Excel.Application();
                xlWorkBook = xlApp.Workbooks.Open(@fullpath, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
              
                range = xlWorkSheet.UsedRange;
                rw = range.Rows.Count;
                cl = range.Columns.Count;
                List<string> lstRecipient = new List<string>();
                List<string> lstAttachment = new List<string>();
                string _Commande = "";
                string _Client = "";
                string _MailClient = "";
                string _NoFacture = "";
                string _Date = "";

                int mailCounter = 0;
                int startCount = 2;
                for (rCnt = startCount; rCnt <= rw; rCnt++)
                {
                    _Date = (string)(range.Cells[rCnt, 1] as Excel.Range).Value2;
                    _Commande = (string)(range.Cells[rCnt, 4] as Excel.Range).Value2;

                    object value = (range.Cells[rCnt, 2] as Excel.Range).Value2;
                    if (value != null)
                    {
                        if (value is double)
                        {
                            _Client = value.ToString();
                        }
                        else
                        {
                            _Client = value.ToString();
                        }
                    }
                    //_Client 
                    _MailClient = (string)(range.Cells[rCnt, 3] as Excel.Range).Value2;
                    _NoFacture = (string)(range.Cells[rCnt, 10] as Excel.Range).Value2;
                
                    ExcelResult.Add(new Tuple<string, string, string, string, string>(_Date, _Client, _MailClient, _NoFacture, _Commande));
                    ExcelResult = ExcelResult.OrderBy(i => i.Item2).ToList();
                }

                //Distinct
                var emails = ExcelResult.GroupBy(t => t.Item2).Select(y => y.First()).ToList();
                Dictionary<string, string> _dates = new Dictionary<string, string>();
                Dictionary<string, string> _attachments = new Dictionary<string, string>();
                numRows = emails.Count;
                lblToBeGenerated.Text = numRows.ToString();
                int startingIndex = 0 + numMail;
                for (var i = startingIndex; i < emails.Count; i++)
                { 
                    foreach (var er in ExcelResult)
                    {
                        if (emails[i].Item2 == er.Item2) {
                            if (_dates.ContainsKey(er.Item2))
                            {
                                _dates[er.Item2] = _dates[er.Item2] + "," + er.Item1;
                            }
                            else
                            {
                                _dates.Add(er.Item2,er.Item1);
                            }

                            if (_attachments.ContainsKey(er.Item2))
                            {
                                _attachments[er.Item2] = _attachments[er.Item2] + "," + er.Item4;
                            }
                            else
                            {
                                _attachments.Add(er.Item2, er.Item4);
                            }
                        }                    
                    }
                    if (numMail < batch)
                    {
                        //Emails.Add(new Tuple<string, string, string, string, string>(_dates[email.Item2], email.Item2, email.Item3, email.Item4, email.Item5));
                        GenerateMail(_dates[emails[i].Item2], emails[i].Item5, emails[i].Item2, emails[i].Item3, _attachments[emails[i].Item2]);
                        mailCounter++;
                        numMail++;
                    }
                }

                batch = batch + mailCounter;
                lblGenerated.Text = numMail.ToString();
                lblRemaining.Text = (numRows - numMail).ToString();
                lblStat.Text = numMail.ToString() + " mails générés";
                MessageBox.Show(lblStat.Text, "Nombre d'e-mails générés?");
                if (numMail < numRows)
                {
                    btnBrowse.Enabled = false;
                    ButtonMail.Text = "Continuer";
                }
                else
                {
                    btnBrowse.Enabled = true;
                    ButtonMail.Text = "Générer";
                }
                xlApp.Quit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Le fichier Excel n'est pas au bon format.");
            }

        }

        private static string ReadSignature()
        {
            string appDataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Microsoft\\Signatures";
            string signature = string.Empty;
            DirectoryInfo diInfo = new DirectoryInfo(appDataDir);

            if (diInfo.Exists)
            {
                FileInfo[] fiSignature = diInfo.GetFiles("*.htm");

                if (fiSignature.Length > 0)
                {
                    StreamReader sr = new StreamReader(fiSignature[0].FullName, Encoding.Default);
                    signature = sr.ReadToEnd();

                    if (!string.IsNullOrEmpty(signature))
                    {
                        string fileName = fiSignature[0].Name.Replace(fiSignature[0].Extension, string.Empty);
                        signature = signature.Replace(fileName + "_files/", appDataDir + "/" + fileName + "_files/");
                    }
                }
            }
            return signature;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            nbDeMail.Items.Add(20);
            nbDeMail.Items.Add(30);
            nbDeMail.Items.Add(40);
            nbDeMail.Items.Add(50);
            nbDeMail.Items.Add(60);

            nbDeMail.SelectedItem = nbDeMail.Items[0];
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void lblFilename_Click(object sender, EventArgs e)
        {

        }
    }
}