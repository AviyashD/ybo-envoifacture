﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using System.IO;
using System.Diagnostics;
using System.Threading;

namespace OutlookMailCreator
{
    public partial class Form1 : Form
    {

        public static String dataExcelPath;
        public static String dataFolder="";
        public static System.Windows.Forms.Label label1;

        public int numMail = 0;
        public int numRows = 0;
        public static List<Tuple<string, string, string, string, string>> ExcelResult = new List<Tuple<string, string, string, string, string>>();


        public Form1()
        {
            InitializeComponent();
            this.Text = "Envoi automatique des factures";
            this.FormClosing += Form1_FormClosing;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (!e.Cancel)
                {
                    if ((numRows - numMail) > 0)
                    {
                        // Assume that X has been clicked and act accordingly.
                        // Confirm user wants to close
                        switch (MessageBox.Show(this, "Vous avez " + (numRows - numMail).ToString() + " mails qui ne sont pas encore générés. Voulez - vous vraiment fermer ?", "Êtes-vous sûr?", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                        {
                            //Stay on this form
                            case DialogResult.No:
                                e.Cancel = true;
                                break;
                            default:
                                break;
                        }
                    }                    
                }
            }

        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {

            try
            {
                
                //lblFilename.Text = AttachFile();
                txtFileName.Text = AttachFile();
                lblStat.Text = "";
                numRows = 0;
                numMail = 0;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
        private  string AttachFile()
        {
            try
            {
                OpenFileDialog attachment = new OpenFileDialog();

                attachment.Title = "Select an excel file";
                attachment.Filter = "Excel files (*.xls or .xlsx)|.xls;*.xlsx";
                attachment.ShowDialog();
                dataExcelPath = attachment.FileName;
                string fileName = attachment.SafeFileName;
                dataFolder = dataExcelPath.Replace(fileName, "");
                return fileName;
            }
            catch(Exception ex)
            {
                //  MessageBox.Show(ex.Message);
                //lblFilename.Text = "Aucun dossier n'est choisi";
                txtFileName.Text = "Aucun dossier n'est choisi";
                dataFolder = "";
                return "Aucun dossier n'est choisi";
            }

           
        }
        private void ButtonMail_Click(object sender, EventArgs e)
        {
            try
            {                
                if (dataFolder!="" && dataFolder != "Aucun dossier n'est choisi")
                {
                    this.ButtonMail.Enabled = false;
                  ProcessExcel(dataExcelPath);
                }
                else
                {
                    MessageBox.Show("Aucun dossier n'est choisi!", "Statut dossier", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                this.ButtonMail.Enabled = true;
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
          
            //string p = getFilePath("oz");
        }

        public static string GetFilePath(string fileName)
        {
            try
            {
                var filePath = System.IO.Directory.GetFiles(@dataFolder).Where(file => file.ToLower().Contains(fileName.ToLower())).FirstOrDefault();
                return filePath;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;

            }


        }

        public void GenerateMail(string _date, string _consultant,string _client, string _mailClient, string _noFacture)
        {
            try
            {
                Outlook.Application outlookApp = new Outlook.Application();
                Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                //Outlook.Inspector oInspector = oMailItem.GetInspector;
                // Thread.Sleep(10000);

                Outlook.Accounts accounts = outlookApp.Session.Accounts;
                foreach (Outlook.Account account in accounts)
                {
                    // When the e-mail address matches, send the mail.
                    Debug.WriteLine("-------------");
                    Debug.WriteLine(comboBox1.SelectedItem);
                    Debug.WriteLine("-------------");
                    // When the e-mail address matches, send the mail.
                    if (account.SmtpAddress.ToString() == comboBox1.SelectedItem.ToString())
                    {
                        oMailItem.SendUsingAccount = account;
                    }
                }

                //Add Subject
                oMailItem.Subject= "[PORTIFY][" + _consultant + "] – Facture";

                //Date
                var finalMonthYear = "";
                var lstDate = new List<string>();
                CultureInfo ci = new CultureInfo("fr-FR");
                if (_date.Contains(','))
                {
                    lstDate = _date.Split(',').ToList();
                }
                else
                {
                    lstDate.Add(_date);
                }
                lstDate = lstDate.GroupBy(t => t).Select(y => y.First()).ToList();
                foreach (var d in lstDate)
                {
                    DateTime strDate = DateTime.Parse(d); 
                    finalMonthYear += " " + strDate.ToString("MMMM",ci) + " " + strDate.Year;
                }

                //Add Body
                oMailItem.Body =
                    "<p>Bonjour,</p>" +
                    "<p>Vous trouverez ci-joint la facture pour la prestation de <b style='font-weight:bolder;'>" + _consultant + "</b>" +
                    " pour le(s) mois de " + finalMonthYear + "</p>" +
                    "<p>En vous remerciant par avance.</p>" +
                    "<p>" + "Cordialement," + "</p>" +
                    "<p>Portify</p>" + "<p><a href='clients@portify.fr'>Clients@portify.fr</a></p>" +
                    "<img src='https://www.adira.org/wp-content/uploads/2019/05/10674_Banniere_LinkedIn.png'/>";

                var lstRecipient = new List<string>();
                if (_mailClient.Contains(','))
                {
                    lstRecipient = _mailClient.Split(',').ToList();
                }
                else
                {
                    lstRecipient.Add(_mailClient);
                }

                // Recipient
                Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;
                foreach (String recipient in lstRecipient)
                {
                    Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(recipient.Trim());
                    oRecip.Resolve();
                }

                //Add Attachment
                string filePath = "";
                List<string> lstAttachment = new List<string>();
                if (_noFacture.Contains(','))
                {
                    lstAttachment = _noFacture.Split(',').ToList();
                }
                else
                {
                    lstAttachment.Add(_noFacture);
                }

                foreach (string attachment in lstAttachment)
                {
                    //string attachmentN = attachment;
                    filePath = GetFilePath(attachment.Trim());
                    if (filePath != null)
                        oMailItem.Attachments.Add(filePath, Outlook.OlAttachmentType.olByValue);
                }

                //Add CC             
                Outlook.Recipient oCCRecip = oRecips.Add("clients@portify.fr");

                oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                oCCRecip.Resolve();

                oMailItem.HTMLBody = oMailItem.Body;
                oMailItem.BodyFormat = Outlook.OlBodyFormat.olFormatHTML;
                if (oMailItem.Attachments.Count > 0)
                {
                    ////Display the mailbox
                    ////oMailItem.Display(oMailItem);
                    ((Outlook._MailItem)oMailItem).Send();
                } else
                {
                    numRows--;
                    numMail--;
                }
                
                //Outlook.Application.Quit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
        public void ProcessExcel(string fullpath)
        {

            try
            {
                

                lblStat.Text = "En Cours...";
                Excel.Application xlApp;
                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                Excel.Range range;
                //fullpath = "C:\\avinash\\OutlookMailCreator\\inputExcel.xlsx";
                string str;
                int rCnt;
                int cCnt;
                int rw = 0;
                int cl = 0;


                xlApp = new Excel.Application();
                xlWorkBook = xlApp.Workbooks.Open(@fullpath, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
              
                range = xlWorkSheet.UsedRange;
                rw = range.Rows.Count;
                cl = range.Columns.Count;

                //MessageBox.Show(range.ToString());
                //MessageBox.Show(rw.ToString());
                //MessageBox.Show(cl.ToString());
                List<string> lstRecipient = new List<string>();
                List<string> lstAttachment = new List<string>();
                string _Consultant = "";
                string _Client = "";
                string _MailClient = "";
                string _NoFacture = "";
                string _Date = "";

                int mailCounter = 0;
                int startCount = 2;
                for (rCnt = startCount; rCnt <= rw; rCnt++)
                {

                   Excel.Range rg = (Excel.Range)xlWorkSheet.Cells[
                       rCnt, 1];

                    object value = rg.Value2;
                    DateTime dt = DateTime.Now;
                    if (value != null)
                    {
                        if (value is double)
                        {
                            dt = DateTime.FromOADate((double)value);
                        }
                        else
                        {
                            DateTime.TryParse((string)value, out dt);
                        }
                    }
                   
                    Debug.WriteLine(dt.ToString());
                    _Date = dt.ToString();
                    _Consultant = (string)(range.Cells[rCnt, 2] as Excel.Range).Value2;
                    _Client = (string)(range.Cells[rCnt, 3] as Excel.Range).Value2;
                    _MailClient = (string)(range.Cells[rCnt, 4] as Excel.Range).Value2;
                    _NoFacture = (string)(range.Cells[rCnt, 13] as Excel.Range).Value2;


                    if (_MailClient != null)
                    {
                        ExcelResult.Add(new Tuple<string, string, string, string, string>(_Date.ToString(), _Client, _MailClient, _NoFacture, _Consultant));
                        ExcelResult = ExcelResult.OrderBy(i => i.Item4).ToList();
                    }

                }

                //Distinct
                var emails = ExcelResult.GroupBy(t => t.Item4).Select(y => y.First()).ToList();
                Dictionary<string, string> _dates = new Dictionary<string, string>();
                numRows = emails.Count;
                lblToBeGenerated.Text = numRows.ToString();
                int startingIndex = 0;// + numMail;
                for (var i = startingIndex; i < emails.Count; i++)
                { 
                    foreach (var er in ExcelResult)
                    {
                        if (emails[i].Item4 == er.Item4) {
                            if (_dates.ContainsKey(er.Item4))
                            {
                                _dates[er.Item4] = _dates[er.Item4] + "," + er.Item1;
                            }
                            else
                            {
                                _dates.Add(er.Item4,er.Item1);
                            }                            
                        }
                    
                    }
                   
                    GenerateMail(_dates[emails[i].Item4], emails[i].Item5, emails[i].Item2, emails[i].Item3, emails[i].Item4);
                    mailCounter++;
                    numMail++;                    
                }
                lblToBeGenerated.Text = numRows.ToString();
                lblGenerated.Text = numMail.ToString();
                lblStat.Text = numMail.ToString() + " mails envoyés";
                MessageBox.Show(lblStat.Text, "Nombre d'e-mails envoyés?");
                if (numMail < numRows)
                {
                    btnBrowse.Enabled = false;
                }
                else
                {
                    btnBrowse.Enabled = true;
                }
                xlApp.Quit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Le fichier Excel n'est pas au bon format.");
                
            }

        }

        private static string ReadSignature()
        {
            string appDataDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Microsoft\\Signatures";
            string signature = string.Empty;
            DirectoryInfo diInfo = new DirectoryInfo(appDataDir);

            if (diInfo.Exists)
            {
                FileInfo[] fiSignature = diInfo.GetFiles("*.htm");

                if (fiSignature.Length > 0)
                {
                    StreamReader sr = new StreamReader(fiSignature[0].FullName, Encoding.Default);
                    signature = sr.ReadToEnd();

                    if (!string.IsNullOrEmpty(signature))
                    {
                        string fileName = fiSignature[0].Name.Replace(fiSignature[0].Extension, string.Empty);
                        signature = signature.Replace(fileName + "_files/", appDataDir + "/" + fileName + "_files/");
                    }
                }
            }
            return signature;
        }

        private void lblFilename_Click(object sender, EventArgs e)
        {

        }

        private void GeneratedMail_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Outlook.Application outlookApp = new Outlook.Application();
            Outlook.Accounts accounts = outlookApp.Session.Accounts;
            foreach (Outlook.Account account in accounts)
            {
                // When the e-mail address matches, send the mail.
                comboBox1.Items.Add(account.SmtpAddress);
                Debug.WriteLine("-------------");
                Debug.WriteLine(account.CurrentUser.Address);
                Debug.WriteLine(account.CurrentUser.AddressEntry.Name);
                Debug.WriteLine(account.CurrentUser.Name);
                Debug.WriteLine(account.CurrentUser.Type);
                Debug.WriteLine(account.DisplayName);
                Debug.WriteLine(account.SmtpAddress);
                Debug.WriteLine(account.UserName);
                Debug.WriteLine("-------------");
               
            }
            comboBox1.SelectedItem = comboBox1.Items[0];
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Environment.Exit(1);
        }
    }
}