﻿namespace OutlookMailCreator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ButtonMail = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.lblStat = new System.Windows.Forms.Label();
            this.mailToBeGenerated = new System.Windows.Forms.Label();
            this.GeneratedMail = new System.Windows.Forms.Label();
            this.lblGenerated = new System.Windows.Forms.Label();
            this.lblToBeGenerated = new System.Windows.Forms.Label();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonMail
            // 
            this.ButtonMail.Location = new System.Drawing.Point(231, 215);
            this.ButtonMail.Name = "ButtonMail";
            this.ButtonMail.Size = new System.Drawing.Size(115, 23);
            this.ButtonMail.TabIndex = 0;
            this.ButtonMail.Text = "Envoyer";
            this.ButtonMail.UseVisualStyleBackColor = true;
            this.ButtonMail.Click += new System.EventHandler(this.ButtonMail_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(480, 105);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(24, 20);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // lblStat
            // 
            this.lblStat.AutoSize = true;
            this.lblStat.Location = new System.Drawing.Point(265, 188);
            this.lblStat.Name = "lblStat";
            this.lblStat.Size = new System.Drawing.Size(0, 13);
            this.lblStat.TabIndex = 3;
            // 
            // mailToBeGenerated
            // 
            this.mailToBeGenerated.AutoSize = true;
            this.mailToBeGenerated.Location = new System.Drawing.Point(54, 131);
            this.mailToBeGenerated.Name = "mailToBeGenerated";
            this.mailToBeGenerated.Size = new System.Drawing.Size(143, 13);
            this.mailToBeGenerated.TabIndex = 4;
            this.mailToBeGenerated.Text = "Nombre d\'e-mails à envoyer: ";
            // 
            // GeneratedMail
            // 
            this.GeneratedMail.AutoSize = true;
            this.GeneratedMail.Location = new System.Drawing.Point(61, 157);
            this.GeneratedMail.Name = "GeneratedMail";
            this.GeneratedMail.Size = new System.Drawing.Size(136, 13);
            this.GeneratedMail.TabIndex = 5;
            this.GeneratedMail.Text = "Nombre d\'e-mails envoyés: ";
            this.GeneratedMail.Click += new System.EventHandler(this.GeneratedMail_Click);
            // 
            // lblGenerated
            // 
            this.lblGenerated.AutoSize = true;
            this.lblGenerated.Location = new System.Drawing.Point(213, 157);
            this.lblGenerated.Name = "lblGenerated";
            this.lblGenerated.Size = new System.Drawing.Size(0, 13);
            this.lblGenerated.TabIndex = 6;
            // 
            // lblToBeGenerated
            // 
            this.lblToBeGenerated.AutoSize = true;
            this.lblToBeGenerated.Location = new System.Drawing.Point(213, 131);
            this.lblToBeGenerated.Name = "lblToBeGenerated";
            this.lblToBeGenerated.Size = new System.Drawing.Size(0, 13);
            this.lblToBeGenerated.TabIndex = 7;
            // 
            // txtFileName
            // 
            this.txtFileName.Location = new System.Drawing.Point(203, 105);
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.Size = new System.Drawing.Size(271, 20);
            this.txtFileName.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(117, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Fichier entrant: ";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(7, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(230, 41);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(203, 78);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(271, 21);
            this.comboBox1.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(117, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Compte email:";
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(571, 249);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtFileName);
            this.Controls.Add(this.lblToBeGenerated);
            this.Controls.Add(this.lblGenerated);
            this.Controls.Add(this.GeneratedMail);
            this.Controls.Add(this.mailToBeGenerated);
            this.Controls.Add(this.lblStat);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.ButtonMail);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "[PORTIFY] Envoie Facture";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonSendMail;
        private System.Windows.Forms.Button ButtonMail;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label lblStat;
        private System.Windows.Forms.Label mailToBeGenerated;
        private System.Windows.Forms.Label GeneratedMail;
        private System.Windows.Forms.Label lblGenerated;
        private System.Windows.Forms.Label lblToBeGenerated;
        private System.Windows.Forms.TextBox txtFileName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
    }
}

